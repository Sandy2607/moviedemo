package com.example.moviedemo.movie.activity;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RadioGroup;
import android.widget.Toast;
import com.example.moviedemo.R;
import com.example.moviedemo.databinding.ActivityMainBinding;
import com.example.moviedemo.movie.adapter.MovieListingAdapter;
import com.example.moviedemo.movie.model.MMovie;
import com.example.moviedemo.movie.service.GetPopularMoviesTask;
import com.example.moviedemo.room.DatabaseClient;
import com.example.moviedemo.util.Constant;
import com.google.android.material.bottomsheet.BottomSheetDialog;
import java.util.ArrayList;
import java.util.List;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.DefaultItemAnimator;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

public class MovieListingActivity extends AppCompatActivity implements Constant, MovieListingAdapter.CallBack, GetPopularMoviesTask.CallBacks {
    private Activity activity;
    private ActivityMainBinding binding;
    private List<MMovie> movieList = new ArrayList<>();
    private MovieListingAdapter movieListingAdapter;
    private Integer pageNo = 1,totalPage;
    private BottomSheetDialog bottomSheetDialog;
    private String ratingText="";
    private boolean loadmore=false;
    private GridLayoutManager layoutManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity = this;
        binding = DataBindingUtil.setContentView(activity, R.layout.activity_main);
        loadMovieData();
        initialization();
        listner();
    }

    private void loadMovieData() {
        loadmore=true;
        new GetPopularMoviesTask().getPopularMoviesTask(activity, pageNo, this);
    }

    private void initialization() {
        bottomSheetDialog = new BottomSheetDialog(activity, R.style.Theme_MaterialComponents_Light_BottomSheetDialog);
        layoutManager=new GridLayoutManager(activity,3);
        binding.rvMovieList.setLayoutManager(layoutManager);
        binding.rvMovieList.setItemAnimator(new DefaultItemAnimator());
        movieListingAdapter = new MovieListingAdapter(activity, movieList, this);
        binding.rvMovieList.setAdapter(movieListingAdapter);
        // deleting table if exist
        DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                .movieDao()
                .deleteTable();
    }

    private void listner() {
        binding.imgFilter.setOnClickListener(view -> openFilter());
        binding.rvMovieList.addOnScrollListener(new RecyclerView.OnScrollListener() {
            @Override
            public void onScrollStateChanged(@NonNull RecyclerView recyclerView, int newState) {
                super.onScrollStateChanged(recyclerView, newState);
            }

            @Override
            public void onScrolled(@NonNull RecyclerView recyclerView, int dx, int dy) {
                super.onScrolled(recyclerView, dx, dy);
                int visibleItemCount = layoutManager.getChildCount();
                int totalItemCount = layoutManager.getItemCount();
                int firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition();

                if (loadmore==true)
                    return;
                    if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                            && firstVisibleItemPosition >= 0) {
                        if (pageNo < totalPage) {
                            pageNo++;
                            loadMovieData();
                        }
                    }

            }
        });
    }

    private void openFilter() {

        View v = LayoutInflater.from(activity).inflate(R.layout.filter_layout, null);
        bottomSheetDialog.setContentView(v);
        bottomSheetDialog.show();
        ((RadioGroup)v.findViewById(R.id.rgRating)).setOnCheckedChangeListener((radioGroup, checkedId) -> {
            switch (checkedId){
                case R.id.rbLowToHigh:
                    ratingText=LOW_TO_HIGH;
                    break;
                case R.id.rbHighToLow:
                    ratingText=HIGH_TO_LOW;
                    break;
            }
        });
        v.findViewById(R.id.btnCancel).setOnClickListener(view -> bottomSheetDialog.dismiss());
        v.findViewById(R.id.btnSubmit).setOnClickListener(view ->{
            applyFilter();
            bottomSheetDialog.dismiss();
        });
    }

    private void applyFilter() {
        movieList.clear();
        movieList.addAll(ratingText.equals(LOW_TO_HIGH)?DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().movieDao().getFilteredListInAscending():DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().movieDao().getFilteredListInDescending());
        notifyAdapter();
    }

    @Override
    public void onSuccessfullyGetPopularMovies(List<MMovie> movieList, Integer total_pages) {
        DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().movieDao().insert(movieList);
        if (pageNo!=1)
        this.movieList.clear();
        else totalPage=total_pages;
        this.movieList.addAll(DatabaseClient.getInstance(getApplicationContext()).getAppDatabase().movieDao().getAll());
        notifyAdapter();
        loadmore=false;
    }

    @Override
    public void onFailedToGetPopularMovies(String message) {
        Toast.makeText(activity, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void openMovieDetailScreen(MMovie mMovie) {
        startActivity(new Intent(activity, MovieDetailActivity.class).putExtra(MOVIE_ID,mMovie.getId()));
    }

    private void notifyAdapter() {
        movieListingAdapter.notifyDataSetChanged();
    }
}
