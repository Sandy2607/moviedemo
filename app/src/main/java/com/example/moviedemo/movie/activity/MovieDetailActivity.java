package com.example.moviedemo.movie.activity;

import android.app.Activity;
import android.os.Bundle;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.moviedemo.R;
import com.example.moviedemo.databinding.ActivityMovieDetailsBinding;
import com.example.moviedemo.movie.model.MMovie;
import com.example.moviedemo.room.DatabaseClient;
import com.example.moviedemo.util.Constant;

import androidx.appcompat.app.AppCompatActivity;
import androidx.databinding.DataBindingUtil;

public class MovieDetailActivity extends AppCompatActivity implements Constant {
    private Activity activity;
    private ActivityMovieDetailsBinding binding;
    private MMovie mMovie=new MMovie();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activity=this;
        binding= DataBindingUtil.setContentView(activity, R.layout.activity_movie_details);
        initialization();
    }

    private void initialization() {
        setSupportActionBar(binding.toolbar);
        binding.toolbarLayout.setExpandedTitleMarginStart(5);
        binding.toolbarLayout.setTitle(getResources().getString(R.string.movie_detail));
//        mMovie=  getIntent().getParcelableExtra(MOVIE_ID);
        mMovie= DatabaseClient.getInstance(getApplicationContext()).getAppDatabase()
                .movieDao()
                .getSelectedMovieDetail(getIntent().getIntExtra(MOVIE_ID,0));
        setData();
    }

    private void setData() {
        Glide.with(activity).load(IMAGE_BASE_URL+mMovie.getBackdrop_path()).apply(new RequestOptions().placeholder(R.drawable.ic_movie_loader).error(R.mipmap.ic_launcher)).into( binding.imgMovie);
        binding.detailContent.tvMovieName.setText(mMovie.getOriginal_title());
        binding.detailContent.tvMovieDate.setText(mMovie.getRelease_date());
        binding.detailContent.tvMovieDetail.setText(mMovie.getOverview());
//        binding.detailContent.tvVoteCount.setText(mMovie.getVote_count()!=null?mMovie.getVote_count():0);
    }
}
