package com.example.moviedemo.movie.model;

import androidx.room.Entity;
import androidx.room.PrimaryKey;
import lombok.Data;

@Data
@Entity
public class MMovie  {
    @PrimaryKey(autoGenerate = true)
    private Integer idd;
    private Integer id;
    private Integer vote_count;
    private Boolean video;
    private Double vote_average;
    private String title;
    private Double popularity;
    private String poster_path;
    private String original_language;
    private String original_title;
    private String backdrop_path;
    private Boolean adult;
    private String overview;
    private String release_date;

    public MMovie() {}
}
