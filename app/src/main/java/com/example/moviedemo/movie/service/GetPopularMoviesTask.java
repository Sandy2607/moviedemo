package com.example.moviedemo.movie.service;

import android.app.Activity;

import com.example.moviedemo.R;
import com.example.moviedemo.apiCalling.APIClient;
import com.example.moviedemo.apiCalling.APIInterface;
import com.example.moviedemo.apiCalling.model.ResponseData;
import com.example.moviedemo.customview.CircularLoading;
import com.example.moviedemo.movie.model.MMovie;
import com.example.moviedemo.util.Constant;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class GetPopularMoviesTask implements Constant {
    private APIInterface apiInterface;
    private Activity activity;
    private CallBacks callBacks;
    private CircularLoading loading;


    public void getPopularMoviesTask(Activity activity,Integer pageNo, CallBacks callBacks) {
        this.activity = activity;
        this.callBacks = callBacks;
        apiInterface = APIClient.getClient(activity).create(APIInterface.class);
        loading = new CircularLoading(activity);
        loading.show(activity.getResources().getString(R.string.loading));
        performGetPopularMovies(pageNo);
    }

    private void performGetPopularMovies(Integer pageNo) {
        Call<ResponseData<List<MMovie>>> callGetPopularMovies =apiInterface.getPopularMovieList(API_KEY,LANGUAGE,pageNo);

        callGetPopularMovies.enqueue(new Callback<ResponseData<List<MMovie>>>() {
            @Override
            public void onResponse(Call<ResponseData<List<MMovie>>> call, Response<ResponseData<List<MMovie>>> response) {
                loading.dismiss();
                if (response.isSuccessful()) callBacks.onSuccessfullyGetPopularMovies(response.body().getResults(),response.body().getTotal_pages());
                else try {
                    callBacks.onFailedToGetPopularMovies(response.message());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            @Override

            public void onFailure(Call<ResponseData<List<MMovie>>> call, Throwable t) {
                loading.dismiss();
                callBacks.onFailedToGetPopularMovies(t.getMessage());
                t.printStackTrace();
            }
        });
    }

    public interface CallBacks {

        void onSuccessfullyGetPopularMovies(List<MMovie> results, Integer total_pages);

        void onFailedToGetPopularMovies(String message);
    }
}