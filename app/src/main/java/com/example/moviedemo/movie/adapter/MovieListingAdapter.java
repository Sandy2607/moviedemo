package com.example.moviedemo.movie.adapter;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.ViewGroup;

import com.bumptech.glide.Glide;
import com.bumptech.glide.request.RequestOptions;
import com.example.moviedemo.R;
import com.example.moviedemo.databinding.ItemMovieLayoutBinding;
import com.example.moviedemo.movie.model.MMovie;
import com.example.moviedemo.util.Constant;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.databinding.DataBindingUtil;
import androidx.recyclerview.widget.RecyclerView;

public class MovieListingAdapter extends RecyclerView.Adapter<MovieListingAdapter.ViewHolder> implements Constant {
    private Activity activity;
    private List<MMovie> movieList;
    private CallBack callBack;

    public MovieListingAdapter(Activity activity, List<MMovie> movieList, CallBack callBack) {
        this.activity=activity;
        this.movieList=movieList;
        this.callBack=callBack;
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        ItemMovieLayoutBinding binding = DataBindingUtil.inflate(LayoutInflater.from(activity), R.layout.item_movie_layout, parent, false);
        return new ViewHolder(binding);
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {
        MMovie mMovie=movieList.get(position);
        holder.binding.tvMovieName.setText(mMovie.getOriginal_title());
        holder.binding.tvDate.setText(mMovie.getRelease_date());
        Glide.with(activity).load(IMAGE_BASE_URL+mMovie.getPoster_path()).apply(new RequestOptions().placeholder(R.drawable.ic_movie_loader).error(R.mipmap.ic_launcher)).into( holder.binding.imgMovie);
        holder.itemView.setOnClickListener(view -> callBack.openMovieDetailScreen(mMovie));
    }

    @Override
    public int getItemCount() {
        return movieList.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ItemMovieLayoutBinding binding;
        public ViewHolder(@NonNull ItemMovieLayoutBinding itemView) {
            super(itemView.getRoot());
            binding=itemView;
        }
    }

    public interface CallBack {
        void openMovieDetailScreen(MMovie mMovie);
    }
}
