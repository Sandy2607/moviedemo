package com.example.moviedemo.util;

public interface Constant {
    String BASE_URL="https://api.themoviedb.org/3/";
    String IMAGE_BASE_URL="http://image.tmdb.org/t/p/w500";
    String API_KEY="976db1b1d13174bfc2f37b7864ca739b";
    String LANGUAGE="en-US";
    String MOVIE_ID ="MOVIE_ID";
    String HIGH_TO_LOW ="HIGH_TO_LOW";
    String LOW_TO_HIGH ="LOW_TO_HIGH";
}
