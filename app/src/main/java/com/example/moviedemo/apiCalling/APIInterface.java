package com.example.moviedemo.apiCalling;


import com.example.moviedemo.apiCalling.model.ResponseData;
import com.example.moviedemo.movie.model.MMovie;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface APIInterface {

     @GET("movie/popular")
    Call<ResponseData<List<MMovie>>> getPopularMovieList(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

}
