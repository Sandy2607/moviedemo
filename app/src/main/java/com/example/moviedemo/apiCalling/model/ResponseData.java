package com.example.moviedemo.apiCalling.model;


import lombok.Data;

@Data
public class ResponseData<T> {
    private Integer page;
    private Integer total_results;
    private Integer total_pages;
    private T results;
}
