package com.example.moviedemo.apiCalling;

import android.app.Activity;
import android.widget.Toast;

import com.example.moviedemo.util.Utils;

import java.io.IOException;

import okhttp3.Interceptor;
import okhttp3.Request;
import okhttp3.Response;


public class ConnectivityInterceptor implements Interceptor {
    private Activity activity;


    public ConnectivityInterceptor(Activity context) {
        activity = context;
    }

    @Override
    public Response intercept(Chain chain) throws IOException {
        if (!Utils.isConnectingToInternet(activity)) {
            throw new NoConnectivityException();
        } else {
            Request request = chain.request();
            Response response = chain.proceed(request);
            return response;
        }
    }

}
