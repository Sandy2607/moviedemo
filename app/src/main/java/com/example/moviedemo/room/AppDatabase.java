package com.example.moviedemo.room;

import com.example.moviedemo.movie.model.MMovie;

import androidx.room.Database;
import androidx.room.RoomDatabase;

@Database(entities = {MMovie.class}, version = 2,exportSchema = false)
//@TypeConverters({Converters.class})
public abstract class AppDatabase extends RoomDatabase {
    public abstract MovieDao movieDao();
}
