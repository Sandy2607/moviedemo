package com.example.moviedemo.room;

import com.example.moviedemo.movie.model.MMovie;

import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface MovieDao {
    @Query("SELECT * FROM mmovie")
    List<MMovie> getAll();

    @Query("SELECT * FROM mmovie WHERE id=:movieId")
    MMovie getSelectedMovieDetail(Integer movieId);

    @Insert
    void insert(List<MMovie> mmovie);

    @Query("DELETE FROM mmovie")
    void deleteTable();

    @Query("SELECT * FROM mmovie ORDER BY vote_average ASC")
    List<MMovie> getFilteredListInAscending();

    @Query("SELECT * FROM mmovie ORDER BY vote_average DESC")
    List<MMovie> getFilteredListInDescending();
}