package com.example.moviedemo.customview;

import android.app.Activity;
import android.app.Dialog;
import android.view.View;
import android.view.Window;
import android.widget.TextView;

import com.example.moviedemo.R;


public class CircularLoading {
    private Activity activity;
    private Dialog dialog;

    public CircularLoading(Activity activity) {
        this.activity = activity;
    }

    public void show(String message) {
        dialog = new Dialog(activity);
        try {
            dialog.getWindow().requestFeature(Window.FEATURE_NO_TITLE);
            dialog.getWindow().setBackgroundDrawableResource(android.R.color.transparent);
            View view = activity.getLayoutInflater().inflate(R.layout.circular_loading, null);
            TextView tvMessage = view.findViewById(R.id.tvMsg);
            tvMessage.setText(message);
            dialog.setContentView(view);
            dialog.getWindow().setLayout(-1, -2);
            dialog.setCancelable(false);
            dialog.show();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }


    public void dismiss() {
        if (dialog != null && dialog.isShowing()) {
            dialog.dismiss();
        }
    }
    public boolean isVisible(){
        return dialog != null && dialog.isShowing();
    }
}
